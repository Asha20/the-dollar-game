const query = location.search
  .slice(1)
  .split("&")
  .map(pair => pair.split("="))
  .reduce((acc, [key, value]) => (acc[key] = value, acc), {});

const rootURL = location.origin + location.pathname;
const maxGameId = 10 ** 6;

function newGame(id = Math.floor(Math.random() * maxGameId)) {
  location.replace(rootURL + "?id=" + id);
}

if (query.id === undefined) {
  newGame();
} else {
  Math.seedrandom(query.id);
  main();
}

import Graph from "./Graph";

function main() {
  const qs = document.querySelector.bind(document);
  
  const DOM = {
    moves: qs(".moves"),
    newGame: qs(".new-game"),
  };
  
  function renderMoves(movesMade) {
    DOM.moves.textContent = movesMade;
  }
  
  function announceVictory(movesMade) {
    const movesPlural = movesMade === 1 ? "move" : "moves";
    alert(`Won game in ${movesMade} ${movesPlural}. Press Space to play another game.`);
  }
  
  const graph = Graph.generateRandom({
    vertices: {min: 5, max: 12},
    edges: {min: 0.2, max: 0.4},
    values: {min: -4, max: 5},
  }, announceVictory, renderMoves);
  
  const game = document.getElementById("game");
  graph.render(game);

  window.addEventListener("keydown", e => {
    if (e.code === "Space") {
      newGame();
    }
  });

  DOM.newGame.addEventListener("click", () => newGame());
}
