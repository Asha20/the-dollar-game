const path = require("path");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const config = require("./webpack.config");

const ROOT = path.resolve(__dirname, "..");

module.exports = {
  ...config,
  mode: "production",
  devtool: "source-map",
  plugins: [
    ...config.plugins,
    new CleanWebpackPlugin(["dist"], {
      root: ROOT,
      verbose: true,
    }),
  ],
};
