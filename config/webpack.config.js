const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const ROOT = path.resolve(__dirname, "..");

module.exports = {
  mode: "none",
  context: ROOT,
  entry: {
    vendor: ["@babel/polyfill", "svg.js", "seedrandom"],
    client: ["./src/index"],
  },
  output: {
    path: path.resolve(ROOT, "dist"),
    filename: "[name].[chunkhash].bundle.js",
    chunkFilename: "[name].[chunkhash].bundle.js",
  },
  resolve: {
    extensions: [".js", ".json"],
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader",
      },
    ],
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: "index.ejs",
    }),
  ],
};
